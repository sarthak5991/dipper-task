$(document).ready( function() {

	//Initializations at document Ready	
	$(".ui.right.dropdown.icon.item").dropdown({
		transition: 'drop',
		on: 'click'
	});

	$(".ui.dropdown.button").dropdown();

	$(".checkslider").attr("data-content","Per Ton");

	$(".ui.input input").val(1);

	//function for Quantity Input field only having numeric input
	$(".ui.input").children("input").keypress( function(event) {
		console.log("inside keypress function");
		// onKeyPress(event,this);

		if(event.which >= 48 && event.which <= 57) {
			console.log("keypress - inside first if");
			
		}
		else {
			event.preventDefault();
		}
		console.log("keypress - " + $(this).val() )

		//to make sure quantity never gets more than 100
		if ( ( ($(this).val())*10 + event.which-48 ) >= 100 ) {						
			console.log("keypress - inside second if")
			event.preventDefault();
		}

	});


	$(".ui.input").children("input").on("input", function(event) {
		var currentValueHashed = $(this).closest(".ui.four.wide.column.card").find(".hashed-price").text();
		var currentValueUnhashed = $(this).closest(".ui.four.wide.column.card").find(".unhashed-price").text();

		console.log("keypress - currHashed " + currentValueHashed);
		console.log("keypress - currUnhashed " + currentValueUnhashed);

		var value = $(this).closest(".row").find("select").val()
		console.log("OnInput - " + value);

		if ( value == "1") {
			console.log("oninput - entered if");
			currentValueHashed = 99 * $(this).val();
			currentValueUnhashed = 95 * $(this).val();
		}
		else {
			console.log("oninput - entered else");
			currentValueHashed = 30 * $(this).val();
			currentValueUnhashed = 29 * $(this).val();
		}

		console.log("keypress - newHashed " + currentValueHashed);
		console.log("keypress - nweUnhashed " + currentValueUnhashed);

		$(this).closest(".ui.four.wide.column.card").find(".hashed-price").html(currentValueHashed);
		$(this).closest(".ui.four.wide.column.card").find(".unhashed-price").html(currentValueUnhashed);
	});


	$(".ui.dropdown.button").change(function(event) {
		var value = $(this).closest(".row").find("select").val();
		console.log("button value - " + value);

		if (value == 2) {
			$(this).closest(".ui.four.wide.column.card").find("input").val(1);
			$(this).closest(".ui.four.wide.column.card").find(".hashed-price").html("30");
			$(this).closest(".ui.four.wide.column.card").find(".unhashed-price").html("29");
			$(this).closest(".ui.four.wide.column.card").find("h4.ui.header").html("Tetrapack 200ml");
		}
		else {
			$(this).closest(".ui.four.wide.column.card").find("input").val(1);
			$(this).closest(".ui.four.wide.column.card").find(".hashed-price").html("99");
			$(this).closest(".ui.four.wide.column.card").find(".unhashed-price").html("95");
			$(this).closest(".ui.four.wide.column.card").find("h4.ui.header").html("Tetrapack 1.0 Lt");
		}

	});

	//function for fly to cart animation
	$(".ui.fluid.red.button").on("click", function() {
		//Scroll to top if cart icon is hidden on top
        // $('html, body').animate({
        //     'scrollTop' : $(".ui.fluid.red.button").position().top
        // });

        var itemImg = $(this).closest(".ui.four.wide.column.card").find("img").eq(0);
        flyToElement( $(itemImg), $('#cart-dropdown') );

        var title = $(this).closest('.ui.four.wide.column.card').find("h3.ui.header").text() + $(this).closest('.ui.four.wide.column.card').find("h4.ui.header").text();
        $(".ui.top.attached.menu .menu").append('<div class="ui item"> <span class="text"> ' + title + ' </span> </div>')
	});

	//function for toggle of Checkbox Slider
	$(".checkslider label").on("click", function(event) {
		console.log("click event");

		if($(".checkslider input").is(':checked')) {
			console.log("entering if");
			$(".checkslider").attr("data-content", "Per Ton");
			$(".checkslider").animate({
				backgroundColor: "#ff6138" 
			}, 500, 'linear', function() {});	
		}
		else {
			console.log("entering else");
			$(".checkslider").attr("data-content", "Per Truck");
			$(".checkslider").animate({
				backgroundColor: "#00a388" 
			}, 500, 'linear', function() {});	
		}
	});

	

});

var flyToElement = function(flyer, flyingTo) {
	var $func = $(this);
	var divider = 3;
	var flyerClone = $(flyer).clone();
	$(flyerClone).css({
		position: 'absolute',
		top: $(flyer).offset().top + "px", 
		left: $(flyer).offset().left + "px",
		opacity: 1,
		'z-index': 10
	});
	$('body').append($(flyerClone));

	var gotoX = $(flyingTo).offset().left + ($(flyingTo).width() / 2) - ($(flyer).width()/divider)/2;
	var gotoY = $(flyingTo).offset().top + ($(flyingTo).height() / 2) - ($(flyer).height()/divider)/2;

	$(flyerClone).animate({
		opacity: 04,
		left: gotoX,
		top: gotoY,
		width: $(flyer).width()/divider,
		height: $(flyer).height()/divider

	}, 700, function() {
		$(flyingTo).fadeOut('fast', function() {
			$(flyingTo).fadeIn('fast', function() {
				$(flyerClone).fadeOut('fast', function() {
					$(flyerClone).remove();
				});
			});
		});
	});
};
